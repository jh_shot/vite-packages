# Html

ViteJS `index.html` 渲染处理插件

## 安装

```bash
npm i @ttou/vite-html
```

## 配置

> `index.html`

```html
<!doctype html>
<html lang="en">
  <head>
    <meta charset="UTF-8" />
    <link rel="icon" type="image/svg+xml" href="/vue.svg" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <!-- 标题 -->
    <%- injectTitle %>
  </head>
  <body>
    <div id="app"></div>
    <script type="module" src="/src/main.ts"></script>
  </body>
</html>
```

> `vite.config.ts`

```js
import { customHtml } from '@ttou/vite-html'
import { defineConfig } from 'vite'

export default defineConfig(() => {
  return {
    plugins: [
      vue(),
      customHtml({
        injectTitle: `<title>Playground</title>`
      })
    ]
  }
})
```
