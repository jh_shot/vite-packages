# @ttou/vite-html

## 0.0.6

### Patch Changes

- 修复插件返回类型

## 0.0.5

### Patch Changes

- 添加打包产物

## 0.0.4

### Patch Changes

- 增加 vite >= 5.0.0 支持

## 0.0.3

### Patch Changes

- 修复 \_\_dirname 未定义

## 0.0.2

### Patch Changes

- 锁定生产依赖

## 0.0.1

### Patch Changes

- 初始化
