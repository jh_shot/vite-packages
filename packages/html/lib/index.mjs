import { dirname as o } from "node:path";
import { fileURLToPath as t } from "node:url";
import m from "ejs";
const n = typeof __dirname < "u" ? __dirname : o(t(import.meta.url));
function a(r) {
  return {
    name: "custom:html",
    enforce: "pre",
    transformIndexHtml: {
      order: "pre",
      handler(e) {
        return m.render(e, { ...r }, { root: n });
      }
    }
  };
}
export {
  a as customHtml
};
