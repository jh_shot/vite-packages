import { Plugin } from 'vite';

/**
 * index.html 处理插件
 * @param options 写法参照 ejs 文档
 * @see https://ejs.co/#docs
 */
export declare function customHtml(options: Record<string, any>): Plugin;
