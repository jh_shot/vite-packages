import { dirname } from 'node:path'
import { fileURLToPath } from 'node:url'

import ejs from 'ejs'
import { Plugin } from 'vite'

/**
 * __dirname 兼容性写法
 * @link https://antfu.me/posts/isomorphic-dirname
 */
const root =
  typeof __dirname !== 'undefined'
    ? __dirname
    : dirname(fileURLToPath(import.meta.url))

/**
 * index.html 处理插件
 * @param options 写法参照 ejs 文档
 * @see https://ejs.co/#docs
 */
export function customHtml(options: Record<string, any>): Plugin {
  return {
    name: 'custom:html',
    enforce: 'pre',
    transformIndexHtml: {
      order: 'pre',
      handler(html) {
        return ejs.render(html, { ...options }, { root })
      }
    }
  }
}
